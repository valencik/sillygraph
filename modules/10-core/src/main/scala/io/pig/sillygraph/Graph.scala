package io.pig.sillygraph
package graph

import java.io._
import scala.collection.mutable.ListBuffer
import scala.collection.immutable.ListMap

class Graph[A] private (
    private val _nodes: Map[Int, A],
    val edges: Vector[(Int, Int)]
) {
  def nodes: Vector[A]                 = _nodes.values.toVector
  def nodesWithIndex: Vector[(Int, A)] = _nodes.toVector
  val size                             = nodes.size

  def filterNodes(p: A => Boolean): Graph[A] = {
    val nodesAndIndex      = _nodes.filter { case (_, a) => p(a) }
    val iNodeSet: Set[Int] = nodesAndIndex.keySet
    val newEdges           = edges.filter { case (x, y) => iNodeSet.contains(x) && iNodeSet.contains(y) }
    new Graph(nodesAndIndex, newEdges)
  }

  def filterEdgeCount(p: Int => Boolean): Graph[A] = {
    val edgeSet  = edgeCounts.filter { case (_, c) => p(c) }.toVector.map(_._1).toSet
    val newEdges = edges.filter(edgeSet.contains(_))
    new Graph(_nodes, newEdges)
  }

  def neighbours(node: A): Vector[A] = {
    val ith = nodesWithIndex.find { case (_, a) => a == node }.map(_._1).getOrElse(-1)
    val iNodes: Vector[Int] =
      if (ith >= 0)
        edges.map { case (x, y) =>
          if (x == ith)
            y
          else if (y == ith)
            x
          else -1
        }
      else Vector.empty[Int]
    for (i <- iNodes if i >= 0 && i < size) yield _nodes(i)
  }
  def edgeCounts: Map[(Int, Int), Int] = edges.groupBy(identity).view.mapValues(_.size).toMap
  def edgeCountsWithNodes: Map[(A, A), Int] = edgeCounts.map { case ((x, y), c) =>
    ((_nodes(x), _nodes(y)), c)
  }
}
object Graph {
  private def empty[A]: Graph[A] = new Graph[A](Map.empty[Int, A], Vector.empty[(Int, Int)])
  @inline private def indexedMap[A](xs: Seq[A]): Map[Int, A] =
    xs.view.zipWithIndex.map { case (a, i) => (i, a) }.toMap
  def apply[A](nodes: Vector[A])(edges: Vector[(Int, Int)]): Graph[A] = {
    new Graph[A](indexedMap(nodes), edges)
  }
  def apply[A](nodes: List[A])(edges: List[(Int, Int)]): Graph[A] = {
    new Graph[A](indexedMap(nodes), edges.toVector)
  }
  def fromAdjacentNodes[A](nodes: List[A]) = {
    if (nodes.isEmpty) empty[A]
    else {
      var iNodes    = ListMap[A, Int](nodes.head -> 0)
      val edges     = new ListBuffer[(Int, Int)]
      var these     = nodes.tail
      var prev: Int = 0
      var ith: Int  = 0
      while (!these.isEmpty) {
        val focus = these.head
        if (iNodes.contains(focus))
          // a b [a] c
          edges += ((prev, iNodes(focus)))
        else {
          // a b a [c]
          ith += 1
          iNodes += focus -> ith
          edges += ((prev, ith))
        }
        these = these.tail
        prev = iNodes(focus)
      }
      new Graph[A](iNodes.map { case (a, i) => (i, a) }, edges.toVector)
    }
  }
}

object GraphUtils {
  import scala.io.Source

  private def arrayToList(array: String): List[String] =
    array.trim.stripPrefix("[").stripSuffix("]").split(", ").toList
  private def arrayToList(start: String, array: String, end: String): List[String] = {
    val nodes = arrayToList(array).appended(end).toList
    start :: nodes
  }
  def fromSQLArray(array: String): Graph[String] = {
    val adjacentNodes = arrayToList(array)
    Graph.fromAdjacentNodes(adjacentNodes)
  }
  def fromSQLArraysWithStartAndEnds(arrays: List[String]): Graph[String] = {
    val adjacentNodes = arrays.map(arrayToList("start", _, "end"))
    Graph.fromAdjacentNodes(adjacentNodes.flatten)
  }
  def readFromFile(filename: String): Graph[String] = {
    val lines = Source.fromFile(filename).getLines().toList
    fromSQLArraysWithStartAndEnds(lines)
  }
  def readListsFromFile(filename: String): List[List[String]] = {
    val lines = Source.fromFile(filename).getLines().toList
    lines.map(arrayToList("start", _, "end"))
  }
  def toDot[A](g: Graph[A]): String = {
    val dotEdges = g.edgeCountsWithNodes.map { case ((x, y), c) =>
      s""""${x}" -> "${y}" [ label = "${c}" ];"""
    }
    //TODO empty
    s"""
    |digraph graph64 {
    |  rankdir=LR;
    |  size="8,5"
    |  node [shape = circle];
    |  ${dotEdges.mkString("\n  ")}
    |}
    |""".stripMargin.trim
  }
  def saveDotFile[A](filename: String, g: Graph[A]): Unit = {
    val file = new File(filename)
    val bw   = new BufferedWriter(new FileWriter(file))
    bw.write(toDot(g))
    bw.close()
  }
}
