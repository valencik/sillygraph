ThisBuild / scalaVersion := "2.13.8"

val munitVersion = "0.7.29"

lazy val commonSettings = Seq(
  organization := "io.pig",
  version := "0.1.0-SNAPSHOT",
  libraryDependencies ++= Seq(
    "org.scalameta" %% "munit" % munitVersion % Test
  )
  //scalacOptions in Compile ~= filterConsoleScalacOptions
)

lazy val root = (project in file("."))
  .aggregate(core)

lazy val core = (project in file("modules/10-core"))
  .settings(commonSettings)
  .settings(
    name := "sillygraph-core",
    testFrameworks += new TestFramework("munit.Framework")
  )
